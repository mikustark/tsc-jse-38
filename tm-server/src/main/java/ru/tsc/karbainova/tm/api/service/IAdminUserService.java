package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IAdminUserService {
    void clear();

    void addAll(Collection<User> users);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    List<User> findAll();

    User findByEmail(@NonNull String email);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    void removeById(String id);

    void removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);
}
