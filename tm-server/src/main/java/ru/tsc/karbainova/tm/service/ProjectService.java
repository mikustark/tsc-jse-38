package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyIndexException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    public ProjectService(IConnectionService connectionService) {
        super(connectionService);
    }

    public IProjectRepository getRepository(@NonNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(userId, project);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(userId, project);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@NonNull String userId, @NonNull Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (project == null) throw new ProjectNotFoundException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(userId, project);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (project == null) throw new ProjectNotFoundException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.remove(userId, project);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Project updateById(@NonNull String userId, @NonNull String id,
                              @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            Project project = projectRepository.findById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setName(name);
            project.setDescription(description);
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Project updateByIndex(@NonNull String userId, @NonNull Integer index,
                                 @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            if (index > projectRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            final Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            project.setName(name);
            project.setDescription(description);
            projectRepository.update(project);
            connection.commit();
            return project;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NonNull String userId, @NonNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            if (index > projectRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            projectRepository.removeByIndex(userId, index);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.removeByName(userId, name);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @NonNull
    @SneakyThrows
    public Project findByIndex(@NonNull String userId, @NonNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            if (index > projectRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            return projectRepository.findByIndex(userId, index);
        } finally {
            connection.close();
        }
    }

    @Override
    @NonNull
    @SneakyThrows
    public Project findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            return projectRepository.findByName(userId, name);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Project startById(@NonNull String userId, @NonNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            final Project project = projectRepository.findById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setStatus(Status.IN_PROGRESS);
            connection.commit();
            return project;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Project startByIndex(@NonNull String userId, @NonNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();

        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            if (index > projectRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            final Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            project.setStatus(Status.IN_PROGRESS);
            connection.commit();
            return project;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Project startByName(@NonNull String userId, @NonNull String name) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            final Project project = projectRepository.findByName(userId, name);
            if (project == null) throw new ProjectNotFoundException();
            project.setStatus(Status.IN_PROGRESS);
            connection.commit();
            return project;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Project finishById(@NonNull String userId, @NonNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            final Project project = projectRepository.findById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setStatus(Status.COMPLETE);
            project.setFinishDate(new Date());
            connection.commit();
            return project;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Project finishByIndex(@NonNull String userId, @NonNull Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            if (index > projectRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            final Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            project.setStatus(Status.COMPLETE);
            project.setFinishDate(new Date());
            connection.commit();
            return project;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Project finishByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IProjectRepository projectRepository = getRepository(connection);
            final Project project = projectRepository.findByName(userId, name);
            if (project == null) throw new ProjectNotFoundException();
            project.setStatus(Status.COMPLETE);
            project.setFinishDate(new Date());
            connection.commit();
            return project;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

//    @Override
//    @SneakyThrows
//    public void addAll(Collection<Project> collection) {
//        if (collection == null) return;
//        @NonNull final Connection connection = connectionService.getConnection();
//        try {
//            @NonNull final IProjectRepository projectRepository = getRepository(connection);
//            projectRepository.addAll(collection);
//        } catch (@NonNull final Exception e) {
//            connection.rollback();
//            throw e;
//        } finally {
//            connection.close();
//        }
//    }


}
