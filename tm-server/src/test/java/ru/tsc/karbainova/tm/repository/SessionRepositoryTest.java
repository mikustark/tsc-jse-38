package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.service.ConnectionService;
import ru.tsc.karbainova.tm.service.PropertyService;

public class SessionRepositoryTest {
    private SessionRepository sessionRepository;
    private Session session;
    private final String userLogin = "test";

    @Before
    public void before() {
        sessionRepository = new SessionRepository(new ConnectionService(new PropertyService()).getConnection());
        sessionRepository.add(new Session());
        session = sessionRepository.findAll().get(0);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());

        @NonNull final boolean s = sessionRepository.findAll().contains(session);
        Assert.assertTrue(s);
    }

    @Test
    public void findAll() {
        Assert.assertTrue(sessionRepository.findAll().size() >= 1);
    }

}
